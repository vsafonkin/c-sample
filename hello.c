#include <stdio.h>
#include <unistd.h>
#include <math.h>

#define SEP printf("%s\n", "------");


void test();

int main() {
    test();
    SEP;
    for (int hour = 0; hour < 1; hour++) {
        for (int min = 0; min < 2; min++) {
            for (int sec = 0; sec < 60; sec++) {
                printf("%02d:%02d:%02d\r", hour, min, sec);
                fflush(stdout);
                sleep(1);
            }
        }
    }
    return 0;    
}

void test() {
    printf("Hello!\n");
}
